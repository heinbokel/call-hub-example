class MockToken {
  userName: string;
  firstName: string;
  lastName: string;
  department: string;
  role: string;
}
