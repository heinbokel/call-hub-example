import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from "../services/authorization.service";
import {UserCredentials} from "../models/UserCredentials";

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})
export class TokenComponent implements OnInit {
  mockToken: MockToken;

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit() {
    this.getToken()
  }

  getToken(){
    const userCredentials: UserCredentials = new UserCredentials();
    userCredentials.password = "password";
    userCredentials.userName = "user";
    this.authorizationService.getToken(userCredentials).subscribe(
      token => this.mockToken = token
    );
  }
}
